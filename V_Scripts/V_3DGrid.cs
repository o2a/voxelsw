﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DigitalRuby.FastLineRenderer;

public class V_3DGrid : MonoBehaviour {


    private void DoGrid(FastLineRenderer Static_FSLineRenderer) {
        FastLineRendererProperties props = new FastLineRendererProperties {
            Radius = 2.0f
        };
        Bounds gridBounds = new Bounds();

        // *** Note: For a 2D grid, pass a value of true for the fill parameter for optimization purposes ***

        // draw a grid cube without filling
        gridBounds.SetMinMax(new Vector3(-2200.0f, -1000.0f, 1000.0f), new Vector3(-200.0f, 1000.0f, 3000.0f));
        Static_FSLineRenderer.AppendGrid(props, gridBounds, 250, false);

        // draw a grid cube with filling
        gridBounds.SetMinMax(new Vector3(200.0f, -1000.0f, 1000.0f), new Vector3(2200.0f, 1000.0f, 3000.0f));
        Static_FSLineRenderer.AppendGrid(props, gridBounds, 250, true);

        // commit the changes
        Static_FSLineRenderer.Apply(true);
    }
    //public FastLineRenderer Static_FSLineRenderer;

    public void Draw3dGrid(Vector3 bound1,Vector3 bound2, Color32 GridColor, FastLineRenderer Static_FSLineRenderer) {

        // we don't want to reset the line renderer here, because we are using the same line renderer from the CreateContinuousLineFromList method and we want to keep those points
        //DigitalRuby.FastLineRenderer.FastLineRenderer.ResetAll();// LineRenderer.Reset();
        Static_FSLineRenderer.Reset();

        // generate random start and end points
        List<Vector3> points = new List<Vector3>();
        //for(int i = 0; i < 10; i++) {
        //    float x = Random.Range(-150.0f,150.0f);
        //    float y = Random.Range(-100.0f,100.0f);
        //    points.Add(new Vector3(x,y,0.0f));

        //    x = Random.Range(-150.0f,150.0f);
        //    y = Random.Range(-100.0f,100.0f);
        //    points.Add(new Vector3(x,y,0.0f));
        //}


        float offsetSize = 1.0f;
        float x1 = bound1.x;
        float y1 = bound1.y;
        float z1 = bound1.z;
        float x2 = bound2.x;
        float y2 = bound2.y;
        float z2 = bound2.z;

        int msize = 150;
        if(y2 - y1 / offsetSize < msize) {
            for(float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                points.Add(new Vector3(x1,y1 + yoff,z2));
                points.Add(new Vector3(x2,y1 + yoff,z2));
            }
        }
        if(y2 - y1 / offsetSize < msize) {
            for(float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                points.Add(new Vector3(x1,y1 + yoff,z1));
                points.Add(new Vector3(x2,y1 + yoff,z1));
            }
        }
        if(y2 - y1 / offsetSize < msize) {
            for(float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                points.Add(new Vector3(x1,y1 + yoff,z1));
                points.Add(new Vector3(x1,y1 + yoff,z2));
            }
        }
        if(y2 - y1 / offsetSize < msize) {
            for(float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                points.Add(new Vector3(x2,y1 + yoff,z1));
                points.Add(new Vector3(x2,y1 + yoff,z2));
            }
        }
        if(x2 - x1 / offsetSize < msize) {
            for(float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                points.Add(new Vector3(x1 + xoff,y1,z1));
                points.Add(new Vector3(x1 + xoff,y2,z1));
            }
        }
        if(x2 - x1 / offsetSize < msize) {
            for(float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                points.Add(new Vector3(x1 + xoff,y1,z2));
                points.Add(new Vector3(x1 + xoff,y2,z2));
            }
        }
        if(x2 - x1 / offsetSize < msize) {
            for(float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                points.Add(new Vector3(x1 + xoff,y2,z1));
                points.Add(new Vector3(x1 + xoff,y2,z2));
            }
        }
        if(x2 - x1 / offsetSize < msize) {
            for(float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                points.Add(new Vector3(x1 + xoff,y1,z1));
                points.Add(new Vector3(x1 + xoff,y1,z2));
            }
        }
        if(z2 - z1 / offsetSize < msize) {
            for(float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                points.Add(new Vector3(x1,y1,z1 + zoff));
                points.Add(new Vector3(x2,y1,z1 + zoff));
            }
        }
        if(z2 - z1 / offsetSize < msize) {
            for(float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                points.Add(new Vector3(x1,y2,z1 + zoff));
                points.Add(new Vector3(x2,y2,z1 + zoff));
            }
        }
        if(z2 - z1 / offsetSize < msize) {
            for(float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                points.Add(new Vector3(x2,y1,z1 + zoff));
                points.Add(new Vector3(x2,y2,z1 + zoff));
            }
        }
        if(z2 - z1 / offsetSize < msize) {
            for(float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                points.Add(new Vector3(x1,y1,z1 + zoff));
                points.Add(new Vector3(x1,y2,z1 + zoff));

            }
        }
        /*
        StringBuilder vectorList = new StringBuilder();
        foreach(var item in points) {
            vectorList.AppendLine("points.Add(new Vector3" + item + ");");
               
        }
        Debug.Log(vectorList);
        */
        // animation time per segment - set to 0 to have the whole thing appear at once
        // const float animationTime = 0.004f;
        const float animationTime = 0f;
        // create properties - do this once, before your loop
        // note you don't need to set the join for distinct segments added via the AddLine or AddLines call
        FastLineRendererProperties props = new FastLineRendererProperties();

        Static_FSLineRenderer.AddLines(props,points,(FastLineRendererProperties _props) => {
            // random color
            props.Color = GridColor;

            /*
            DigitalRuby.FastLineRenderer.LineGroupList l = new LineGroupList();

            l.LineRadius = 2.0f;
            l.LineColor = Color.green;
            l.GlowWidthMultiplier = 0.0f;
            l.GlowIntensity = 0.4f;
            l.AddStartCap = true;
            l.AddEndCap = true;
            l.LineJoin = FastLineRendererLineJoin.Round;
            l.Continuous = true;
           */


            // random radius
            props.Radius = Random.Range(0.02f,0.02f);

            // animate this line segment in later
            props.AddCreationTimeSeconds(animationTime);
        },false,false);

        // must call apply to make changes permanent
        Static_FSLineRenderer.Apply();

    }


    public void OnGUI() {

        //cross
        /*
          Vector3 screenPos0 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0f, 0.5f, 0f));
          Vector3 screenPos1 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0f, -0.5f, 0f));
          Vector3 screenPos2 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, 0f, 0f));
          Vector3 screenPos3 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, 0f, 0f));
          Vector3 screenPos4 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0f, 0f, 0.5f));
          Vector3 screenPos5 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0f, 0f, -0.5f));
          Vector3 screenPos6 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0f, -0.5f, 0f));
          Vector3 screenPos7 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0f, 0.5f, 0f));
          */
        /*
                Vector3 screenPos0 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, 0.5f, 0.5f));
                Vector3 screenPos1 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, -0.5f, 0.5f));
                Vector3 screenPos2 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, 0.50f, 0.5f));
                Vector3 screenPos3 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, 0.5f, 0.5f));
                Vector3 screenPos4 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, 0.5f, 0.5f));
                Vector3 screenPos5 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, 0.5f, -0.5f));

                Vector3 screenPos6 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, -0.5f, 0.5f));
                Vector3 screenPos7 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, 0.5f, 0.5f));

                Vector3 screenPos8 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f));
                Vector3 screenPos9 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, 0.5f, -0.5f));
                Vector3 screenPos10 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.50f, -0.5f));
                Vector3 screenPos11 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(0.5f, -0.5f, -0.5f));
                Vector3 screenPos12 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f));
                Vector3 screenPos13 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.5f, 0.5f));
                Vector3 screenPos14 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, 0.5f, -0.5f));
                Vector3 screenPos15 = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f));


                screenPos0.x *= Screen.width; screenPos0.y = (1 - screenPos0.y) * Screen.height;
                screenPos1.x *= Screen.width; screenPos1.y = (1 - screenPos1.y) * Screen.height;
                screenPos2.x *= Screen.width; screenPos2.y = (1 - screenPos2.y) * Screen.height;
                screenPos3.x *= Screen.width; screenPos3.y = (1 - screenPos3.y) * Screen.height;
                screenPos4.x *= Screen.width; screenPos4.y = (1 - screenPos4.y) * Screen.height;
                screenPos5.x *= Screen.width; screenPos5.y = (1 - screenPos5.y) * Screen.height;
                //screenPos6.x *= Screen.width; screenPos6.y = (1 - screenPos6.y) * Screen.height;
                //screenPos7.x *= Screen.width; screenPos7.y = (1 - screenPos7.y) * Screen.height;

                screenPos8.x *= Screen.width; screenPos8.y = (1 - screenPos8.y) * Screen.height;
                screenPos9.x *= Screen.width; screenPos9.y = (1 - screenPos9.y) * Screen.height;
                screenPos10.x *= Screen.width; screenPos10.y = (1 - screenPos10.y) * Screen.height;
                screenPos11.x *= Screen.width; screenPos11.y = (1 - screenPos11.y) * Screen.height;
                screenPos12.x *= Screen.width; screenPos12.y = (1 - screenPos12.y) * Screen.height;
                screenPos13.x *= Screen.width; screenPos13.y = (1 - screenPos13.y) * Screen.height;
                screenPos14.x *= Screen.width; screenPos14.y = (1 - screenPos14.y) * Screen.height;
                screenPos15.x *= Screen.width; screenPos15.y = (1 - screenPos15.y) * Screen.height;


                Drawing.DrawLine(screenPos0, screenPos1, Color.black, 1f, true);
                Drawing.DrawLine(screenPos2, screenPos3, Color.red, 1f, true);
                Drawing.DrawLine(screenPos4, screenPos5, Color.blue, 1f, true);


                //Drawing.DrawLine(screenPos6, screenPos7, Color.green, 1f, true);

                //Drawing.DrawLine(screenPos8, screenPos9, Color.black, 1f, true);


                Drawing.DrawLine(screenPos10, screenPos11, Color.red, 1f, true);
                Drawing.DrawLine(screenPos12, screenPos13, Color.blue, 1f, true);
                Drawing.DrawLine(screenPos14, screenPos15, Color.green, 1f, true);


        */

        /*
        Vector3[] bounds = CalcBounds(SelPos1Cube.transform.position + new Vector3(-0.5f,-0.5f,-0.5f),SelPos2Cube.transform.position + new Vector3(-0.5f,-0.5f,-0.5f));

        float offsetSize = 1f;
                float x1 = bounds[0].x;
                float y1 = bounds[0].y;
                float z1 = bounds[0].z;
                float x2 = bounds[1].x;
                float y2 = bounds[1].y;
                float z2 = bounds[1].z;

                int msize = 150;
                if (y2 - y1 / offsetSize < msize) {
                    for (float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1, y1 + yoff, z2)), World2Screen(
                        new Vector3(x2, y1 + yoff, z2)),
                        Color.blue, 1f, true);
                    }
                }
                if (y2 - y1 / offsetSize < msize) {
                    for (float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1, y1 + yoff, z1)), World2Screen(
                        new Vector3(x2, y1 + yoff, z1)),
                        Color.red, 1f, true);
                    }
                }
                if (y2 - y1 / offsetSize < msize) {
                    for (float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1, y1 + yoff, z1)), World2Screen(
                        new Vector3(x1, y1 + yoff, z2)),
                        Color.gray, 1f, true);
                    }
                }
                if (y2 - y1 / offsetSize < msize) {
                    for (float yoff = 0.0f; yoff + y1 <= y2; yoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x2, y1 + yoff, z1)), World2Screen(
                        new Vector3(x2, y1 + yoff, z2)),
                        Color.cyan, 1f, true);
                    }
                }
                if (x2 - x1 / offsetSize < msize) {
                    for (float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1 + xoff, y1, z1)), World2Screen(
                        new Vector3(x1 + xoff, y2, z1)),
                        Color.cyan, 1f, true);
                    }
                }
                if (x2 - x1 / offsetSize < msize) {
                    for (float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1 + xoff, y1, z2)), World2Screen(
                        new Vector3(x1 + xoff, y2, z2)),
                        Color.cyan, 1f, true);
                    }
                }
                if (x2 - x1 / offsetSize < msize) {
                    for (float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1 + xoff, y2, z1)), World2Screen(
                        new Vector3(x1 + xoff, y2, z2)),
                        Color.cyan, 1f, true);
                    }
                }
                if (x2 - x1 / offsetSize < msize) {
                    for (float xoff = 0.0f; xoff + x1 <= x2; xoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1 + xoff, y1, z1)), World2Screen(
                        new Vector3(x1 + xoff, y1, z2)),
                        Color.cyan, 1f, true);
                    }
                }
                if (z2 - z1 / offsetSize < msize) {
                    for (float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1, y1, z1 + zoff)), World2Screen(
                        new Vector3(x2, y1, z1 + zoff)),
                        Color.cyan, 1f, true);
                    }
                }
                if (z2 - z1 / offsetSize < msize) {
                    for (float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1, y2, z1 + zoff)), World2Screen(
                        new Vector3(x2, y2, z1 + zoff)),
                        Color.cyan, 1f, true);
                    }
                }
                if (z2 - z1 / offsetSize < msize) {
                    for (float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x2, y1, z1 + zoff)), World2Screen(
                        new Vector3(x2, y2, z1 + zoff)),
                        Color.cyan, 1f, true);
                    }
                }
                if (z2 - z1 / offsetSize < msize) {
                    for (float zoff = 0.0f; zoff + z1 <= z2; zoff += offsetSize) {
                        Drawing.DrawLine(World2Screen(
                        new Vector3(x1, y1, z1 + zoff)), World2Screen(
                        new Vector3(x1, y2, z1 + zoff)),
                        Color.cyan, 1f, true);
                    }
                }

               
        */






        /*
          Vector3 screenPos = _mainCam.WorldToViewportPoint(SelPos1Cube.transform.position);
        screenPos.x *= Screen.width;
        screenPos.y = (1-screenPos.y)* Screen.height;

        //screenPos(0.3, 0.5, 93.4)//Flip so its on opposite side  screen 0-1
        //(200.7, 196.7, 93.4) Screen.width:664 Screen.height:374

        transform.position = screenPos;
        Drawing.DrawLine(screenPos, screenPos + new Vector3(0, 5, 0), Color.black, 1f, true);
        Debug.Log(screenPos + " "+Screen.width + " " + Screen.height);
        */

        /*
        Vector3 tPos = SelPos1Cube.transform.position;
        float tX = (tPos.x * Screen.width);
        float tY = (tPos.y * Screen.height);

        Vector3 screenPos = new Vector3(tX, tY, Camera.main.nearClipPlane);
        Vector3 StartPos = Camera.main.ScreenToWorldPoint(screenPos);

        Drawing.DrawLine(StartPos, StartPos + new Vector3(0, 5, 0), Color.black, 5f, true);
        */
        /*
        //Camera.main.WorldToScreenPoint(SelPos1Cube.transform.position);
        //RectTransformUtility.WorldToScreenPoint(Camera.main, SelPos1Cube.transform.position);
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(SelPos1Cube.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * Screen.height) - (Screen.height * 0.5f)),
        ((ViewportPosition.y * Screen.width) - (Screen.width * 0.5f)));
        Drawing.DrawLine(WorldObject_ScreenPosition, Camera.main.WorldToScreenPoint(SelPos1Cube.transform.position) + new Vector3(0, 5, 0), Color.black, 5f, true);
        */

    }






}
