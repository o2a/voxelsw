﻿using UnityEngine;
using Voxelmetric.Code.Data_types;

public class V_Selection : MonoBehaviour {

    public class blockSelection
    {
        public Vector3Int[] block;
        public ushort[] type;
    }


    /// <summary>
    /// Store selection of block positions and type. Better format for serialization
    /// </summary>
    public class Selection {
        public Vector3Int savePos;
        public Vector3Int pos1;
        public Vector3Int pos2;
        public int blockCount;
        public Block[] blocks;
    }

    public class Block {
        public Vector3Int pos;
        public ushort type;
    }
    ////////////////////////////

}
