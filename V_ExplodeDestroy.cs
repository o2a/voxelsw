﻿using UnityEngine;
using System.Collections;
using VE = Voxelmetric.Examples.VoxelmetricExample;
using VM = Voxelmetric.Code.Voxelmetric;
using Voxelmetric.Code.Data_types;
using System.Collections.Generic;

public class V_ExplodeDestroy {

    private float cubeHeight = 1f;
    private float cubeWidth = 1f;
    private float cubeLength = 1f;
    public enum BlockFace {
        top,
        bottom,
        left,
        right,
        forward,
        back
    }


    public Vector3Int[] removeBlocks;

    /// <summary>
    /// Determines if the block is visible at the specified position (Does not check if block exists)
    /// </summary>
    /// <param name="pos">The coordinates</param>
    /// <returns>True if the block can be seen, otherwise false.</returns>
    public bool IsBlockVisible(int x, int y, int z) {
        /*
        int x = Mathf.FloorToInt(pos.x);
        int y = Mathf.FloorToInt(pos.y);
        int z = Mathf.FloorToInt(pos.z);
        */
        //bool top = VM.GetBlock(VE.worldRef,new Vector3(x,y + 1,z)).type == 0;

        bool top = VM.GetBlock(VE.worldRef, new Vector3(x, y + 1, z)).type == 0;
        bool bottom = VM.GetBlock(VE.worldRef, new Vector3(x, y - 1, z)).type == 0;
        bool left = VM.GetBlock(VE.worldRef, new Vector3(x - 1, y, z)).type == 0;
        bool right = VM.GetBlock(VE.worldRef, new Vector3(x + 1, y, z)).type == 0;
        bool forward = VM.GetBlock(VE.worldRef, new Vector3(x, y, z + 1)).type == 0;
        bool back = VM.GetBlock(VE.worldRef, new Vector3(x, y, z - 1)).type == 0;

        return (top || bottom || left || right || forward || back);
    }

    public void DestroyVoxels(int xPos, int yPos, int zPos, int range, bool addSpin) {
        /*
        // Set up a material which we will apply to the cubes which we spawn to replace destroyed voxels.
        Material fakeVoxelMaterial = Resources.Load("Materials/FakeColoredCubes",typeof(Material)) as Material;
        Texture diffuseMap = coloredCubesVolume.GetComponent<ColoredCubesVolumeRenderer>().material.GetTexture("_DiffuseMap");
        if(diffuseMap != null) {
            List<string> keywords = new List<string> { "DIFFUSE_TEXTURE_ON" };
            fakeVoxelMaterial.shaderKeywords = keywords.ToArray();
            fakeVoxelMaterial.SetTexture("_DiffuseMap",diffuseMap);
        }
        fakeVoxelMaterial.SetTexture("_NormalMap",coloredCubesVolume.GetComponent<ColoredCubesVolumeRenderer>().material.GetTexture("_NormalMap"));
        fakeVoxelMaterial.SetFloat("_NoiseStrength",coloredCubesVolume.GetComponent<ColoredCubesVolumeRenderer>().material.GetFloat("_NoiseStrength"));
        */


        // Initialise outside the loop, but we'll use it later.
        Vector3 pos = new Vector3(xPos, yPos, zPos);
        int rangeSquared = range * range;

        // Later on we will be deleting some voxels, but we'll also be looking at the neighbours of a voxel.
        // This interaction can have some unexpected results, so it is best to first make a list of voxels we
        // want to delete and then delete them later in a separate pass.
        List<Vector3Int> voxelsToDelete = new List<Vector3Int>();

        // Iterage over every voxel in a cubic region defined by the received position (the center) and
        // the range. It is quite possible that this will be hundreds or even thousands of voxels.
        for (int z = zPos - range; z < zPos + range; z++) {
            for (int y = yPos - range; y < yPos + range; y++) {
                for (int x = xPos - range; x < xPos + range; x++) {
                    // Compute the distance from the current voxel to the center of our explosion.
                    int xDistance = x - xPos;
                    int yDistance = y - yPos;
                    int zDistance = z - zPos;

                    // Working with squared distances avoids costly square root operations.
                    int distSquared = xDistance * xDistance + yDistance * yDistance + zDistance * zDistance;

                    // We're iterating over a cubic region, but we want our explosion to be spherical. Therefore 
                    // we only further consider voxels which are within the required range of our explosion center. 
                    // The corners of the cubic region we are iterating over will fail the following test.
                    if (distSquared < rangeSquared) {

                        // Get the current color of the voxel
                        //QuantizedColor color = coloredCubesVolume.data.GetVoxel(x,y,z);

                        // Check the alpha to determine whether the voxel is visible. 
                        //if(color.alpha > 127) {
                        Vector3Int voxel = new Vector3Int(x, y, z);
                        voxelsToDelete.Add(voxel);
                        ushort blocktype = VM.GetBlock(VE.worldRef, voxel).type;
                        string blockname = VM.GetBlock(VE.worldRef, voxel).displayName;
                        if (blocktype != 0) {
                            if (IsBlockVisible(x, y, z)) {
                                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                                cube.GetComponent<MeshFilter>().mesh.uv = ApplyUV(blockname);

                                //GameObject cube = Instantiate(VE.VoxelCubeRef);

                                // GameObject cube = new GameObject("ExpCube_"+ blockname);

                                /*cube.AddComponent<MeshRenderer>();
                                MeshFilter filter = cube.AddComponent<MeshFilter>();
                                Mesh mesh = filter.mesh;
                                mesh.vertices = GetVertices();
                                mesh.normals = GetNormals();
                                mesh.uv = ApplyUV(blockname);
                                mesh.triangles = GetTriangles();
                                mesh.RecalculateBounds();
                                mesh.RecalculateNormals();
                                */
                                //Material chunkmat = (Material)Resources.Load("Chunk Material", typeof(Material));
                                Material chunkmat = VE.chunkMatRef;
                                cube.GetComponent<Renderer>().material = chunkmat;

                                //  ApplyUV(ref cube, blockname);
                                Voxelmetric.Code.Load_Resources.Textures.TextureProvider textureProvider = VM.resources.GetTextureProvider(VE.worldRef);
                              
                                cube.GetComponent<MeshRenderer>().material.mainTexture = textureProvider.atlas;

                                cube.AddComponent<BoxCollider>();
                                cube.AddComponent<Rigidbody>();
                                //cube.transform.parent = coloredCubesVolume.transform;
                                cube.transform.localPosition = new Vector3(x, y, z);
                                cube.transform.localRotation = Quaternion.identity;
                                float scale = 0.52f;
                                cube.transform.localScale = new Vector3(scale, scale, scale);
                                //cube.GetComponent<Renderer>().material = fakeVoxelMaterial;
                                //cube.GetComponent<Renderer>().material.SetColor("_CubeColor",(Color32)color);
                                //cube.GetComponent<Renderer>().material.SetVector("_CubePosition",new Vector4(x,y,z,0.0f));

                                Vector3 explosionForce = cube.transform.position - pos;

                                // These are basically random values found through experimentation.
                                // They just add a bit of twist as the cubes explode which looks nice
                                float xTorque = (x * 1436523.4f) % 56.0f;
                                float yTorque = (y * 56143.4f) % 43.0f;
                                float zTorque = (z * 22873.4f) % 38.0f;

                                Vector3 up = new Vector3(0.0f, 2.0f, 0.0f);
                                //cube.AddComponent<SphereCollider>();
                                //cube.GetComponent<BoxCollider>().enabled = false;
                                //cube.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Continuous;

                                if (addSpin) {
                                     cube.GetComponent<Rigidbody>().AddTorque(xTorque, yTorque, zTorque);
                                     cube.GetComponent<Rigidbody>().AddForce((explosionForce.normalized + up) * 100.0f);
                                }
                                // Cubes are just a temporary visual effect, and we delete them after a few seconds.
                                float lifeTime = Random.Range(8.0f, 12.0f);
                               MonoBehaviour.Destroy(cube, lifeTime);
                            }
                        }
                        //}
                    }
                }
            }
        }

        foreach (Vector3Int voxel in voxelsToDelete) // Loop through List with foreach
        {
            VM.SetBlock(VE.worldRef, new Vector3Int(voxel.x, voxel.y, voxel.z), new BlockData(0));
        }
    }


    private Vector3[] GetVertices() {

        Vector3 vertice_0 = new Vector3(-cubeLength * .5f, -cubeWidth * .5f, cubeHeight * .5f);
        Vector3 vertice_1 = new Vector3(cubeLength * .5f, -cubeWidth * .5f, cubeHeight * .5f);
        Vector3 vertice_2 = new Vector3(cubeLength * .5f, -cubeWidth * .5f, -cubeHeight * .5f);
        Vector3 vertice_3 = new Vector3(-cubeLength * .5f, -cubeWidth * .5f, -cubeHeight * .5f);
        Vector3 vertice_4 = new Vector3(-cubeLength * .5f, cubeWidth * .5f, cubeHeight * .5f);
        Vector3 vertice_5 = new Vector3(cubeLength * .5f, cubeWidth * .5f, cubeHeight * .5f);
        Vector3 vertice_6 = new Vector3(cubeLength * .5f, cubeWidth * .5f, -cubeHeight * .5f);
        Vector3 vertice_7 = new Vector3(-cubeLength * .5f, cubeWidth * .5f, -cubeHeight * .5f);
        Vector3[] vertices = new Vector3[]
{
// Bottom Polygon
vertice_0, vertice_1, vertice_2, vertice_3,
// Left Polygon
vertice_7, vertice_4, vertice_0, vertice_3,
// Front Polygon
vertice_4, vertice_5, vertice_1, vertice_0,
// Back Polygon
vertice_6, vertice_7, vertice_3, vertice_2,
// Right Polygon
vertice_5, vertice_6, vertice_2, vertice_1,
// Top Polygon
vertice_7, vertice_6, vertice_5, vertice_4
};

        return vertices;
    }

    /// <summary>
    /// The Cube Side Which Are Use when rendering Cube
    /// </summary>
    /// <returns>The normals.</returns>
    private Vector3[] GetNormals() {
        Vector3 up = Vector3.up;
        Vector3 down = Vector3.down;
        Vector3 front = Vector3.forward;
        Vector3 back = Vector3.back;
        Vector3 left = Vector3.left;
        Vector3 right = Vector3.right;

        Vector3[] normales = new Vector3[]
        {
            // Bottom Side Render
            down, down, down, down,
                    
                // LEFT Side Render
                    left, left, left, left,
                    
                // FRONT Side Render
                    front, front, front, front,
                    
                // BACK Side Render
                    back, back, back, back,
                    
                // RIGTH Side Render
                    right, right, right, right,
                    
                // UP Side Render
                    up, up, up, up
                };

        return normales;
    }


    //void ApplyUV(ref GameObject obj, string textureName) {
        Vector2[] ApplyUV(string textureName) {
           // Mesh mesh = obj.GetComponent<MeshFilter>().mesh;
        Vector2[] UVs = new Vector2[24];

        Vector2[] UV_Top = new Vector2[4];
        Vector2[] UV_Bottom = new Vector2[4];
        Vector2[] UV_Front = new Vector2[4];
        Vector2[] UV_Back = new Vector2[4];
        Vector2[] UV_Right = new Vector2[4];
        Vector2[] UV_Left = new Vector2[4];

        Block blk = VE.worldRef.blockProvider.BlockTypes[VE.worldRef.blockProvider.GetType(textureName)];
        UV_Top = blk.GetBlockTextures(UV_Top, Voxelmetric.Code.Data_types.Direction.up);
        UV_Bottom = blk.GetBlockTextures(UV_Bottom, Voxelmetric.Code.Data_types.Direction.down);
        UV_Front = blk.GetBlockTextures(UV_Front, Voxelmetric.Code.Data_types.Direction.north);
        UV_Back = blk.GetBlockTextures(UV_Back, Voxelmetric.Code.Data_types.Direction.south);
        UV_Right = blk.GetBlockTextures(UV_Right, Voxelmetric.Code.Data_types.Direction.east);
        UV_Left = blk.GetBlockTextures(UV_Left, Voxelmetric.Code.Data_types.Direction.west);
        /*
        Rect uvsFront = new Rect(UV_Front[0].x, UV_Front[0].y, 0.125f, 0.125f );
    public var uvsBack : Rect = new Rect( 0.125, 0.875, 0.125, 0.125 );
    public var uvsLeft : Rect = new Rect( 0.25, 0.75, 0.125, 0.125 );
    public var uvsRight : Rect = new Rect( 0.375, 0.625, 0.125, 0.125 );
    public var uvsTop : Rect = new Rect( 0.5, 0.5, 0.125, 0.125 );
    public var uvsBottom : Rect = new Rect( 0.625, 0.375, 0.125, 0.125 );



    // FRONT    2    3    0    1
    UVs[2] = Vector2(uvsFront.x, uvsFront.y);
        UVs[3] = Vector2(uvsFront.x + uvsFront.width, uvsFront.y);
        UVs[0] = Vector2(uvsFront.x, uvsFront.y - uvsFront.height);
        UVs[1] = Vector2(uvsFront.x + uvsFront.width, uvsFront.y - uvsFront.height);

        // BACK    6    7   10   11
        UVs[6] = Vector2(uvsBack.x, uvsBack.y);
        UVs[7] = Vector2(uvsBack.x + uvsBack.width, uvsBack.y);
        UVs[10] = Vector2(uvsBack.x, uvsBack.y - uvsBack.height);
        UVs[11] = Vector2(uvsBack.x + uvsBack.width, uvsBack.y - uvsBack.height);

        // LEFT   19   17   16   18
        UVs[19] = Vector2(uvsLeft.x, uvsLeft.y);
        UVs[17] = Vector2(uvsLeft.x + uvsLeft.width, uvsLeft.y);
        UVs[16] = Vector2(uvsLeft.x, uvsLeft.y - uvsLeft.height);
        UVs[18] = Vector2(uvsLeft.x + uvsLeft.width, uvsLeft.y - uvsLeft.height);

        // RIGHT   23   21   20   22
        UVs[23] = Vector2(uvsRight.x, uvsRight.y);
        UVs[21] = Vector2(uvsRight.x + uvsRight.width, uvsRight.y);
        UVs[20] = Vector2(uvsRight.x, uvsRight.y - uvsRight.height);
        UVs[22] = Vector2(uvsRight.x + uvsRight.width, uvsRight.y - uvsRight.height);

        // TOP    4    5    8    9
        UVs[4] = Vector2(uvsTop.x, uvsTop.y);
        UVs[5] = Vector2(uvsTop.x + uvsTop.width, uvsTop.y);
        UVs[8] = Vector2(uvsTop.x, uvsTop.y - uvsTop.height);
        UVs[9] = Vector2(uvsTop.x + uvsTop.width, uvsTop.y - uvsTop.height);

        // BOTTOM   15   13   12   14
        UVs[15] = Vector2(uvsBottom.x, uvsBottom.y);
        UVs[13] = Vector2(uvsBottom.x + uvsBottom.width, uvsBottom.y);
        UVs[12] = Vector2(uvsBottom.x, uvsBottom.y - uvsBottom.height);
        UVs[14] = Vector2(uvsBottom.x + uvsBottom.width, uvsBottom.y - uvsBottom.height);

        // - Assign the mesh its new UVs -
        mesh.uv = UVs;
        */

        
        Vector2[] Uv2 = new Vector2[]
{
// Bottom
    //_11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
        UV_Bottom[0],UV_Bottom[1],UV_Bottom[2],UV_Bottom[3],
// Left
    //_11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
        UV_Left[0],UV_Left[1],UV_Left[2],UV_Left[3],
// Front
    //_11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
        UV_Front[0],UV_Front[1],UV_Front[2],UV_Front[3],
// Back
    //_11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
        UV_Back[0],UV_Back[1],UV_Back[2],UV_Back[3],
// Right
    //_11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
    //    UV_Right[0],UV_Right[1],UV_Right[2],UV_Right[3],
        UV_Right[2],UV_Right[3],UV_Right[0],UV_Right[1],
// Top
    //_11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
        UV_Top[0],UV_Top[1],UV_Top[2],UV_Top[3]
};
      //  mesh.uv = Uv2;
     // return Uv2;
        //    2    3    0    1   Front
        //    6    7   10   11   Back
        //   19   17   16   18   Left
        //   23   21   20   22   Right
        //    4    5    8    9   Top
        //   15   13   12   14   Bottom
        
        // Front
        /*
        UVs[0] = UV_Front[1];
        UVs[1] = UV_Front[0];
        UVs[2] = UV_Front[2];
        UVs[3] = UV_Front[3];
        */
        UVs[0] = UV_Front[1];
        UVs[1] = UV_Front[0];
        UVs[2] = UV_Front[2];
        UVs[3] = UV_Front[3];
        // Top
        UVs[4] = UV_Top[1];
        UVs[5] = UV_Top[0];
        UVs[8] = UV_Top[2];
        UVs[9] = UV_Top[3];
        // Back
        UVs[6] = UV_Back[1];
        UVs[7] = UV_Back[0];
        UVs[10] = UV_Back[2];
        UVs[11] = UV_Back[3];
        // Bottom
        UVs[15] = UV_Bottom[1];
        UVs[13] = UV_Bottom[0];
        UVs[12] = UV_Bottom[2];
        UVs[14] = UV_Bottom[3];
        // Left
        UVs[19] = UV_Left[1];
        UVs[17] = UV_Left[0];
        UVs[16] = UV_Left[2];
        UVs[18] = UV_Left[3];
        // Right        
        UVs[23] = UV_Right[1];
        UVs[21] = UV_Right[0];
        UVs[20] = UV_Right[2];
        UVs[22] = UV_Right[3];
        return UVs;
        
        
    }

    private int[] GetTriangles() {
        int[] triangles = new int[]
{
            // Cube Bottom Side Triangles
            3, 1, 0,
            3, 2, 1,	
            // Cube Left Side Triangles
            3 + 4 * 1, 1 + 4 * 1, 0 + 4 * 1,
            3 + 4 * 1, 2 + 4 * 1, 1 + 4 * 1,
            // Cube Front Side Triangles
            3 + 4 * 2, 1 + 4 * 2, 0 + 4 * 2,
            3 + 4 * 2, 2 + 4 * 2, 1 + 4 * 2,
            // Cube Back Side Triangles
            3 + 4 * 3, 1 + 4 * 3, 0 + 4 * 3,
            3 + 4 * 3, 2 + 4 * 3, 1 + 4 * 3,
            // Cube Rigth Side Triangles
            3 + 4 * 4, 1 + 4 * 4, 0 + 4 * 4,
            3 + 4 * 4, 2 + 4 * 4, 1 + 4 * 4,
            // Cube Top Side Triangles
            3 + 4 * 5, 1 + 4 * 5, 0 + 4 * 5,
            3 + 4 * 5, 2 + 4 * 5, 1 + 4 * 5,
            };
        return triangles;
    }
}
