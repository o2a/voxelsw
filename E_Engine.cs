﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Adds the volume command to the console, which allows for setting the global AudioListener volume.
/// </summary>
public class E_Engine : MonoBehaviour {

    string invalidCmd = "<color=#" + Console.ccError + "> Invalid Parameter</color>: ";
    string warningCmd = "<color=#" + Console.ccWarning + "> Warning</color>: ";

    string e_timescaleInfo = "Game Time (0f=Paused - 1f=Realtime) \\ Default: 1f";
    string e_timeInfo = "Time since launching game";

    // Use this for initialization
    void Awake() {
        Console.AddCommand("e_timescale", Console.dash + e_timescaleInfo, e_timescale);
        Console.AddCommand("e_time", Console.dash + e_timeInfo, e_time);
        Console.AddCommand("timescale", Console.dash + e_timescaleInfo, e_timescale);
        Console.AddCommand("time", Console.dash + e_timeInfo, e_time);
    }

    string e_timescale(string param) {

        if (string.IsNullOrEmpty(param)) {
            return e_timescaleInfo + "\nCurrently:" + Time.timeScale;
        }
        if (param.Contains("?")) {
            return " Currently: " + Time.timeScale;
        }
        if (param.Contains("default")) {
            return " Currently: " + Time.timeScale;
        }
        float setTime;
        if (float.TryParse(param, out setTime)) {
            if (setTime >= 0 && setTime <= 1) {
                Time.timeScale = setTime;
                return "<color=#" + Console.ccAccepted + ">Timescale set to</color>: " + setTime;
            }
            return invalidCmd + param + ", " + e_timescaleInfo + "\nCurrently: " + Time.timeScale;
        }

        return invalidCmd + param +", "+ e_timescaleInfo + "\nCurrently: " + Time.timeScale;
    }
    
    string e_time(string param) {
        DateTime dt = DateTime.Now;
        return "Time since game launch: " + ToRelativeString(dt.AddSeconds(-Time.realtimeSinceStartup));
    }

    /// <summary>
    /// Returns a string representation of this date/time. If the
    /// value is close to now, a relative description is returned.
    /// </summary>
    string ToRelativeString(DateTime dt) {
        TimeSpan span = (DateTime.Now - dt);

        // Normalize time span
        bool future = false;
        if (span.TotalSeconds < 0) {
            // In the future
            span = -span;
            future = true;
        }

        // Test for Now
        double totalSeconds = span.TotalSeconds;
        if (totalSeconds < 0.9) {
            return "Now";
        }

        // Date/time near current date/time
        string format = (future) ? "In {0} {1}" : "{0} {1} ago";
        if (totalSeconds < 55) {
            // Seconds
            int seconds = Math.Max(1, span.Seconds);
            return String.Format(format, seconds,
                (seconds == 1) ? "second" : "seconds");
        }

        if (totalSeconds < (55 * 60)) {
            // Minutes
            int minutes = Math.Max(1, span.Minutes);
            return String.Format(format, minutes,
                (minutes == 1) ? "minute" : "minutes");
        }
        if (totalSeconds < (24 * 60 * 60)) {
            // Hours
            int hours = Math.Max(1, span.Hours);
            return String.Format(format, hours,
                (hours == 1) ? "hour" : "hours");
        }

        // Format both date and time
        if (totalSeconds < (48 * 60 * 60)) {
            // 1 Day
            format = (future) ? "Tomorrow" : "Yesterday";
        } else if (totalSeconds < (3 * 24 * 60 * 60)) {
            // 2 Days
            format = String.Format(format, 2, "days");
        } else {
            // Absolute date
            if (dt.Year == DateTime.Now.Year)
                format = dt.ToString(@"MMM d");
            else
                format = dt.ToString(@"MMM d, yyyy");
        }

        // Add time
        return String.Format("{0} at {1:h:mmt}", format, dt);
    }
 
}

