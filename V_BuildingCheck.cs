﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Voxelmetric.Code.Data_types;
using VE = Voxelmetric.Examples.VoxelmetricExample;
using VM = Voxelmetric.Code.Voxelmetric;

public class V_BuildingCheck {

    Vector3Int up = new Vector3Int(0, 1, 0);
    Vector3Int down = new Vector3Int(0, -1, 0);
    Vector3Int north = new Vector3Int(0, 0, 1);
    Vector3Int south = new Vector3Int(0, 0, -1);
    Vector3Int east = new Vector3Int(1, 0, 0);
    Vector3Int west = new Vector3Int(-1, 0, 0);

    int BuildingArea = 0;
    int xx = 0;
    int positionAirStarts = 0;

    public class BuildingCheck {
        public Dictionary<int, Vector3Int> rblock;
        public Dictionary<Vector3Int, int> block;
        public HashSet<int>[] labels;
        public BuildingCheck(int count) {
            rblock = new Dictionary<int, Vector3Int>();
            block = new Dictionary<Vector3Int, int>();
            labels = new HashSet<int>[count];
        }
    }
    //TODO Use seperate thread for checking building structure for detached blocks.
    public void Check(ref V_Selection Building, Vector3Int pos1, Vector3Int pos2) {
        int x, y, z = 0;
        int label = 1;
        bool foundLowerLabel = false;
        Vector3Int tempPos;
        BuildingCheck check = new BuildingCheck(Building.blockCount);
        check.labels = new HashSet<int>[Building.blockCount];
        int i = 0;
        BuildingArea = (Building.xDistance * Building.zDistance) + 2;

#if WATCH
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
#endif
        //Loops from bottom of building to the top
        //for (y = Building.yDistance; y-- > 0;) { //from top > down
        for (y = 0; y < Building.yDistance; y++) { //from bottom > up
            for (x = 0; x < Building.xDistance; x++) {
                for (z = 0; z < Building.zDistance; z++) {
                    tempPos = new Vector3Int(pos1.x + ((pos2.x > pos1.x) ? x : x * -1),
                                             pos1.y + ((pos2.y > pos1.y) ? y : y * -1),
                                             pos1.z + ((pos2.z > pos1.z) ? z : z * -1));
                    //Debug.Log(tempPos + "y: " + y + ", x: " + x + ", z: " + z);
                    ushort blocktype = VM.GetBlock(VE.worldRef, tempPos).type;

                    check.labels[i] = new HashSet<int>();

                    //Bottom of the building, all blocks considered solid and not floating
                    //if (y == Building.yDistance - 1) { //from top > down
                    if (y == 0) { //from bottom > up

                        if (blocktype == 0) {
                            check.block.Add(tempPos, i);
                            check.rblock.Add(i, tempPos);
                            check.labels[i].Add(-1);  //AirBlock

                        } else {
                            check.block.Add(tempPos, i);
                            check.rblock.Add(i, tempPos);
                            check.labels[i].Add(1); //1 = Connected to ground
                        }
                    } else {
                        if (blocktype == 0) {
                            check.block.Add(tempPos, i);
                            check.rblock.Add(i, tempPos);
                            check.labels[i].Add(-1);
                        } else {
                            check.block.Add(tempPos, i);
                            check.rblock.Add(i, tempPos);
                            positionAirStarts = i; //Avoid checking airblocks by storing last position a solid block was found.
                        }
                    }
                    i++;
                } // loop z
            } // loop x
        } // loop y
#if WATCH
        sw.Stop();
        Debug.Log("BlockGet ("+ i + " blocks) took: " + sw.Elapsed.TotalMilliseconds + "ms");
        sw.Reset();

        sw.Start();
#endif
        //https://en.wikipedia.org/wiki/Connected-component_labeling
        //Keep track of destroyed blocks, assign labels to blocks that are
        //not connected to the base structure
        for (i = 0; i < positionAirStarts + 1; i++) {
            if (!check.labels[i].Contains(-1)) {

                foundLowerLabel = false;
                CheckSurrounding(ref check, ref foundLowerLabel, i, label, check.rblock[i] + down);
                CheckSurrounding(ref check, ref foundLowerLabel, i, label, check.rblock[i] + north);
                CheckSurrounding(ref check, ref foundLowerLabel, i, label, check.rblock[i] + south);
                CheckSurrounding(ref check, ref foundLowerLabel, i, label, check.rblock[i] + east);
                CheckSurrounding(ref check, ref foundLowerLabel, i, label, check.rblock[i] + west);

                if (foundLowerLabel == false && check.labels[i].Count == 0) {
                    ++label;
                    check.labels[i].Add(label);
                }
            }
        }
        //Debug.Break();
#if WATCH
        sw.Stop();
        Debug.Log("CheckSurrounding ("+ (positionAirStarts + 1 )*5+ " total checks) took: " + sw.Elapsed.TotalMilliseconds + "ms");
        sw.Reset();
#endif
        /*
        //---------Debugging----------
        i = 0;
        string final = "";
        foreach (var item in check.labels) {
            final += i + check.rblock[i].ToString() +" "+ string.Join(",", item.Select(n => n.ToString()).ToArray()) + "\n";
            i++;
        }
        Debug.Log(final);
        //---------Debugging----------//
        */


#if WATCH
        sw.Start();
#endif
        //---------Destroy all detached blocks that are not touching base structure, labelled (int 1)----------\\
        HashSet<int> temp = new HashSet<int>();
        for (i = 0; i < positionAirStarts + 1; i++) {

            if (check.labels[i].Contains(1)) {
                foreach (var item in check.labels[i]) {
                    temp.Add(item);
                }
            } else {
                foreach (var item in temp) {
                    if (check.labels[i].Contains(item)) {
                        foreach (var item2 in check.labels[i]) {
                            temp.Add(item2);
                        }
                        break;
                        //Break as no need to further check temp as all values in the 
                        //label have been added due to contains match being found.
                    }
                }
            }

        }
#if WATCH
        sw.Stop();
        Debug.Log("Check labels took: " + sw.Elapsed.TotalMilliseconds + "ms");
#endif
        for (i = 0; i < positionAirStarts + 1; i++) {
            bool match = false;
            foreach (var item in check.labels[i]) {
                if (temp.Contains(item)) {
                    match = true; //finding a match means the block is connected to the base structure/
                    break;
                }

                //removes the block as it is not connected to the base structure, 0 = air so cannot be removed anyway.
                if (!match && !check.labels[i].Contains(-1)) {
                    V_ExplodeDestroy ex = new V_ExplodeDestroy();
                    ex.DestroyVoxels(check.rblock[i].x, check.rblock[i].y, check.rblock[i].z, 1, true);
                    break;
                }
            }
        }
        
        return;
    }

    public void CheckSurrounding(ref BuildingCheck check, ref bool foundLowerLabel, int i, int label, Vector3Int blockPos) {
        int labelNum = int.MaxValue;
        if (check.block.ContainsKey(blockPos)) {
            if (check.labels[check.block[blockPos]].Contains(-1)) {
                return;
            }
            foreach (var item in check.labels[check.block[blockPos]]) {
                if (item < labelNum) {
                    labelNum = item;
                }
                if (!check.labels[i].Contains(-1)) {
                    check.labels[i].Add(item);
                }
            }
        } else { return; }
        if (labelNum > 0 && labelNum < label) {
            foundLowerLabel = true;
        }
    }
}