using System;
using Newtonsoft.Json;
using UnityEngine;
using Voxelmetric.Code.Data_types;
using Newtonsoft.Json.Linq;

internal class Vector3IntConverter : JsonConverter {

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {

        if (value == null) {
            writer.WriteNull();
            return;
        }

        var vec = (Vector3Int)value;
        writer.WriteStartObject();
        writer.WritePropertyName("x");
        writer.WriteValue(vec.x);
        writer.WritePropertyName("y");
        writer.WriteValue(vec.y);
        writer.WritePropertyName("z");
        writer.WriteValue(vec.z);
        writer.WriteEndObject();
    }

    public override bool CanConvert(Type objectType) {
        return objectType == typeof(Vector3Int);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
        if (reader.TokenType == JsonToken.Null) {
            return new Vector3Int();
        }
        var obj = JObject.Load(reader);

        if (objectType == typeof(Vector3Int)) {
            return new Vector3Int((int)obj["x"], (int)obj["y"], (int)obj["z"]);
        }

        return new Vector3Int((int)obj["x"], (int)obj["y"], (int)obj["z"]);
    }

    public override bool CanRead {
        get { return true; }
    }

    public override bool CanWrite {
        get { return false; }
    }
}
