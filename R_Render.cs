﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Adds the volume command to the console, which allows for setting the global AudioListener volume.
/// </summary>
public class R_Render : MonoBehaviour {

    string invalidCmd = "<color=#" + Console.ccError + "> Invalid Parameter</color>: ";
    string warningCmd = "<color=#" + Console.ccWarning + "> Warning</color>: ";

    string r_antialiasingInfo = "AntiAlasing (0=Off,1=2x,2=4x,3=8x) \\ Default: 0";
    string r_anisotropicfilteringInfo = "Anisotropic filtering (0=Off,1=Per Texture, 2=ForcedOn \\ Default: 1";
    string r_qualityInfo = "Preset levels(0=Low - 5=Fantastic) \\ Default: 4";
    string r_vsyncInfo = "Vsync count (0=Off, 1=On, 2=Every 2nd VBlank) \\ Default: 0";
    string r_shadowsInfo = "Shadows (0=off 1=1cascade, 2=2cascade)";
    string r_shadowsdistanceInfo = "Shadow distance (0-10000, 2000+ is high)";
    string r_shadowresolutionInfo = "Shadow Resolution (1=Low - 4=VeryHigh)";
    string r_textureresolutionInfo = "Texture Resolution (0-FullRes,1=HalfRes,2=Terrible,3=Minecraft bad)";
    string r_particleraycastlimitInfo = "Particle Raycastlimit 'collision detection limit' (0-4096)";
    

    // Use this for initialization
    void Awake() {
        Console.AddCommand("r_antialiasing", Console.dash + r_antialiasingInfo, r_antialiasing);
        Console.AddCommand("r_anisotropicfiltering", Console.dash + r_anisotropicfilteringInfo, r_anisotropicfiltering);
        Console.AddCommand("r_quality", Console.dash + r_qualityInfo, r_quality);
        Console.AddCommand("r_vsync", Console.dash + r_vsyncInfo, r_vsync);
        //Console.AddCommand("r_shadows", r_shadowsInfo, r_shadows);
        //Console.AddCommand("r_shadowresolution", r_shadowresolutionInfo, r_shadowresolution);
        Console.AddCommand("r_shadowsdistance", Console.dash + r_shadowsdistanceInfo, r_shadowsdistance);
        Console.AddCommand("r_texturequality", Console.dash + r_textureresolutionInfo, r_textureresolution);
        Console.AddCommand("r_particleraycastlimit", Console.dash + r_particleraycastlimitInfo, r_particleraycastlimit);
    }

    string r_antialiasing(string param) {

        if (string.IsNullOrEmpty(param)) {
            return r_antialiasingInfo+"\nCurrently:" + QualitySettings.antiAliasing + "x";
        }

        if (param.Contains("?")) {
            return " Currently: "+QualitySettings.antiAliasing.ToString() + "x";
        }
       
              switch (param) {
                case "0":
                    QualitySettings.antiAliasing = 0;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: 0=Disabled";
                case "1":
                    QualitySettings.antiAliasing = 2;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: 2x";
                case "2x":
                    QualitySettings.antiAliasing = 2;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: "+ param;
                case "2":
                    QualitySettings.antiAliasing = 4;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: 4x";
                case "4x":
                    QualitySettings.antiAliasing = 4;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: " + param;
                case "3":
                    QualitySettings.antiAliasing = 8;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: 8x";
                case "8x":
                    QualitySettings.antiAliasing = 8;
                    return "<color=#" + Console.ccAccepted + ">AntiAliasing set to</color>: " + param;
            }
            
        return invalidCmd + param + ", " + r_antialiasingInfo +"\nCurrently: " + QualitySettings.antiAliasing;
    }

    string r_quality(string param) {

        if (string.IsNullOrEmpty(param)) {
            return r_qualityInfo+"\nCurrently: " + QualitySettings.antiAliasing;
        }

        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.GetQualityLevel().ToString();
        }
        
        int Quality;
        if (int.TryParse(param, out Quality)) {
            if (Quality > -1 && Quality < 6) { 
            QualitySettings.SetQualityLevel(Quality, true);
            return "<color=#" + Console.ccAccepted + ">Quality set to</color>: " + Quality + "x";
            }
            return invalidCmd + param + ", " + r_qualityInfo +"\nCurrently: " + QualitySettings.GetQualityLevel().ToString();
        }
      
        return invalidCmd + param + ", " + r_qualityInfo +"\nCurrently: " + QualitySettings.GetQualityLevel().ToString();
    }

    string r_vsync(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_vsyncInfo+"\nCurrently: " + QualitySettings.antiAliasing;
        }

        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.vSyncCount.ToString();
        }
        int Vsync;
        if (int.TryParse(param, out Vsync)) {
            if (Vsync > -1 && Vsync < 3) {
                QualitySettings.vSyncCount = Vsync;
                return "<color=#" + Console.ccAccepted + ">Vsync set to</color>: " + Vsync + "x";
            }
        }
        return invalidCmd + param + ", " + r_vsyncInfo + "\nCurrently: " + QualitySettings.vSyncCount.ToString();

    }

    string r_shadows(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_vsyncInfo + "\nCurrently: Unsupported";
        }

        if (param.Contains("?")) {
            return " Currently: Unsupported";
        }

        return invalidCmd + param + ", " + r_shadowsInfo + "\nCurrently: Unsupported";

    }

    string r_textureresolution(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_vsyncInfo + "\nCurrently: " + QualitySettings.masterTextureLimit;
        }

        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.masterTextureLimit.ToString();
        }

        int Texture;
        if (int.TryParse(param, out Texture)) {
            switch (Texture) {
                case 0:
                    QualitySettings.masterTextureLimit = Texture;
                    return "<color=#" + Console.ccAccepted + ">Texture resolution set to</color>: " + Texture + " -FullRes";
                case 1:
                    QualitySettings.masterTextureLimit = Texture;
                    return "<color=#" + Console.ccAccepted + ">Texture resolution set to</color>: " + Texture + " -Halfres";
                case 2:
                    QualitySettings.masterTextureLimit = Texture;
                    return "<color=#" + Console.ccAccepted + ">Texture resolution set to</color>: " + Texture + " -QuarterRes/Terrible";
                case 3:
                    QualitySettings.masterTextureLimit = Texture;
                    return "<color=#" + Console.ccAccepted + ">Texture resolution set to</color>: " + Texture + " -EighthRes/Minecraft bad";
            }
        }
        return invalidCmd + param + r_textureresolutionInfo + "\nCurrently: " + QualitySettings.masterTextureLimit.ToString();
    }

    string r_shadowsdistance(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_vsyncInfo + "\nCurrently: " + QualitySettings.shadowDistance;
        }

        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.shadowDistance.ToString();
        }
        int ShadowDistance;
        if (int.TryParse(param, out ShadowDistance)) {
            if (ShadowDistance > -1 && ShadowDistance < 2400) {
                QualitySettings.shadowDistance = ShadowDistance;
                return "<color=#" + Console.ccAccepted + ">Shadow distance set to</color>: " + ShadowDistance;
            }
            if (ShadowDistance > -1 && ShadowDistance < 10000) {
                QualitySettings.shadowDistance = ShadowDistance;
                return "<color=#" + Console.ccAccepted + ">Shadow distance set to</color>: " + ShadowDistance +"\n "+ warningCmd + ": this could be a really high shadow distance";
            }
        }
        return invalidCmd + param + ", " + r_shadowsdistanceInfo + "\nCurrently: " + QualitySettings.shadowDistance.ToString();
    }

    string r_shadowresolution(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_vsyncInfo + "\nCurrently: " + QualitySettings.antiAliasing;
        }

        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.vSyncCount.ToString();
        }

        return invalidCmd + param + ", " + r_shadowresolutionInfo + "\nCurrently: " + QualitySettings.shadowDistance.ToString();
    }
    string r_anisotropicfiltering(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_anisotropicfilteringInfo + "\nCurrently: " + QualitySettings.anisotropicFiltering;
        }
       
        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.anisotropicFiltering;
        }
        int AnisotropicFiltering;
        if (int.TryParse(param, out AnisotropicFiltering)) {
            if (AnisotropicFiltering > -1 && AnisotropicFiltering < 3) {
                QualitySettings.shadowDistance = AnisotropicFiltering;
                return "<color=#" + Console.ccAccepted + ">Anisotropic filtering set to</color>: " + AnisotropicFiltering +"="+ UnityEngine.AnisotropicFiltering.GetName(typeof(UnityEngine.AnisotropicFiltering), AnisotropicFiltering);
            }
        }
        return invalidCmd + param + ", " + r_anisotropicfilteringInfo + "\nCurrently: "+ QualitySettings.anisotropicFiltering.ToString() + "=" + UnityEngine.AnisotropicFiltering.GetName(typeof(UnityEngine.AnisotropicFiltering), QualitySettings.anisotropicFiltering);

    }

    string r_particleraycastlimit(string param) {
        if (string.IsNullOrEmpty(param)) {
            return r_anisotropicfilteringInfo + "\nCurrently: " + QualitySettings.particleRaycastBudget;
        }

        if (param.Contains("?")) {
            return " Currently: " + QualitySettings.particleRaycastBudget;
        }
        int ParticleRaycastBudget;
        if (int.TryParse(param, out ParticleRaycastBudget)) {
            if (ParticleRaycastBudget > -1 && ParticleRaycastBudget < 4096) {
                QualitySettings.particleRaycastBudget = ParticleRaycastBudget;
                return "<color=#" + Console.ccAccepted + ">Particle raycast budget set to</color>: " + ParticleRaycastBudget;
            }
        }
        return invalidCmd + param + ", " + r_anisotropicfilteringInfo + "\nCurrently: " + QualitySettings.particleRaycastBudget;

    }
}

