using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Voxelmetric.Code.Data_types;
using VE = Voxelmetric.Examples.VoxelmetricExample;
using VM = Voxelmetric.Code.Voxelmetric;

public class V_Building : MonoBehaviour {

    public string BuildingToLoad;
    V_Selection Building;
    Bounds gridBounds = new Bounds();
    Vector3Int newPos;
    public BoxCollider boxColl;

    public void Awake() {
        boxColl = GetComponent<BoxCollider>();
    }
    public void TestBuilding() {
        Vector3Int newPos = Building.savePos - (Vector3Int)this.transform.position;
        V_BuildingCheck test = new V_BuildingCheck();
        test.Check(ref Building, Building.pos1 - newPos, Building.pos2 - newPos);
    }
    public void Update() {
        
        newPos = Building.savePos - (Vector3Int)this.transform.position;
        Vector3[] bounds = V_Voxel.CalcBounds((Vector3)(Building.pos1 - newPos) + new Vector3(-0.5f, -0.5f, -0.5f), (Vector3)(Building.pos2 - newPos) + new Vector3(-0.5f, -0.5f, -0.5f));
        gridBounds.SetMinMax(bounds[0], bounds[1]);
        boxColl.size = gridBounds.size;
        boxColl.center = gridBounds.center - this.transform.position;
       
        if (Input.GetKeyDown(KeyCode.T)){
            V_BuildingCheck test = new V_BuildingCheck();
            test.Check(ref Building, Building.pos1 - newPos, Building.pos2 - newPos);
        }
    }

    public void OnEnable() {
        if (loadBuilding()) {
            Vector3Int newPos = Building.savePos - (Vector3Int)this.transform.position;
            Vector3[] bounds = V_Voxel.CalcBounds((Vector3)(Building.pos1 - newPos) + new Vector3(-0.5f, -0.5f, -0.5f), (Vector3)(Building.pos2 - newPos) + new Vector3(-0.5f, -0.5f, -0.5f));
            gridBounds.SetMinMax(bounds[0], bounds[1]);
            
            //Paste building
            for (int i = 0; i <= Building.blocks.Length - 1; i++) {
                
                VM.SetBlock(VE.worldRef, Building.blocks[i].pos - newPos, new BlockData(Building.blocks[i].type));
            };
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(gridBounds.center, gridBounds.size);
    }

    public void OnDisable() {
        if (loadBuilding()) {
            for (int i = 0; i <= Building.blocks.Length - 1; i++) {
                Vector3Int newPos = Building.savePos - (Vector3Int)this.transform.position;
                VM.SetBlock(VE.worldRef, Building.blocks[i].pos - newPos, new BlockData(0));
            };
        }
    }

    public bool loadBuilding() {
        try {
            if (File.Exists(V_Voxel.PlatformPath + BuildingToLoad + ".json")) {

                Building = JsonConvert.DeserializeObject<V_Selection>(File.ReadAllText(V_Voxel.PlatformPath + BuildingToLoad + ".json"));

                Debug.Log("Building '"+ BuildingToLoad + "' Loaded. Block Count: " + Building.blocks.Length);
                return true;
            } else {
                Debug.Log("voxel load: Could not find file at path.. \n" + V_Voxel.PlatformPath + BuildingToLoad + ".json");
            }
        } catch (System.Exception e) {
            Debug.Log("voxel load: " + e);
        }
        return false;
    }
}
