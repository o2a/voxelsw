﻿using UnityEngine;
using System.Collections;
using Voxelmetric.Code.Core;
using Voxelmetric.Code.Data_types;
using Voxelmetric.Code.Load_Resources.Blocks;
using VE = Voxelmetric.Examples.VoxelmetricExample;
using VM = Voxelmetric.Code.Voxelmetric;
using Voxelmetric.Code;
using System.Collections.Generic;
using DigitalRuby.FastLineRenderer;
using System.Text;
using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class V_Voxel : MonoBehaviour {

    public GameObject SelPosCube;
    public static GameObject SelPos1Cube;
    public static GameObject SelPos2Cube;
    [SerializeField]
    public FastLineRenderer FSLineRenderer;
    public static FastLineRenderer Static_FSLineRenderer;
    public FastLineRenderer FSLineRendereronEditable;
    public static bool WE_InProgress = false;
    public int UndoMax = 8;
    public static V_Selection InMemorySelection;

    public static V_3DGrid CuboidGrid = new V_3DGrid();
    public static V_3DGrid CuboidGridNonEditable = new V_3DGrid();

    private Stack<V_Selection> _Undo = new Stack<V_Selection>();
    private Stack<V_Selection> _Redo = new Stack<V_Selection>();
    private V_Selection _tempUndo = new V_Selection();
    private int _undoCurrentPos = 0;
    private bool _undoR = false;

    private GameObject SelPos1CubeNonEditable;
    private GameObject SelPos2CubeNonEditable;


    private static Vector3Int _pos1;
    private static Vector3Int _pos2;
    private static bool _pos1Default = false;
    private static bool _pos2Default = false;
    private VmRaycastHit camPosition;
    private Camera _mainCam;

    private static Vector3Int _pos1_nonEditable;
    private static Vector3Int _pos2_nonEditable;
    private static bool CuboidGridSet = false;
    private static bool CuboidGridNonEditableSet = false;


    public static Vector3Int Pos1 {
        set {
            _pos1 = value;
            _pos1Default = true;
            SelPos1Cube.transform.position = value;
            //Subtracting 0.5f from all axis to align with voxelmetric block placement(center cube, mc anchor is on a corner)
            Vector3[] bounds = CalcBounds(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f), SelPos2Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f));
            //Debug.Log("_pos1" + _pos1);
            //Debug.Log(bounds[0] + " " + bounds[1]);
            //CuboidGrid.Draw3dGrid(bounds[0], bounds[1], new Color32((byte)UnityEngine.Random.Range(222, 256), (byte)UnityEngine.Random.Range(0, 10), (byte)UnityEngine.Random.Range(0, 10), (byte)175), Static_FSLineRenderer);

            Static_FSLineRenderer.Reset();
            FastLineRendererProperties props = new FastLineRendererProperties {
                Radius = 0.02f,
                Color = new Color32(255, 0, 0, 175)
            };
            Bounds gridBounds = new Bounds();
            gridBounds.SetMinMax(bounds[0], bounds[1]);
            Static_FSLineRenderer.AppendGrid(props, gridBounds, 1, false);

            Static_FSLineRenderer.Apply(true);
        }
        get { return _pos1; }
    }
    public static Vector3Int Pos2 {
        set {
            _pos2 = value;
            _pos2Default = true;
            SelPos2Cube.transform.position = value;
            Vector3[] bounds = CalcBounds(SelPos1Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f), SelPos2Cube.transform.position + new Vector3(-0.5f, -0.5f, -0.5f));
            //Debug.Log("_pos2" + _pos2);
            //Debug.Log(bounds[0] + " " + bounds[1]);
            //CuboidGrid.Draw3dGrid(bounds[0], bounds[1], new Color32((byte)UnityEngine.Random.Range(222, 256), (byte)UnityEngine.Random.Range(0, 10), (byte)UnityEngine.Random.Range(0, 10), (byte)175), Static_FSLineRenderer);

            Static_FSLineRenderer.Reset();
            FastLineRendererProperties props = new FastLineRendererProperties {
                Radius = 0.02f,
                Color = new Color32(255, 0, 0, 175)
            };
            Bounds gridBounds = new Bounds();
            gridBounds.SetMinMax(bounds[0], bounds[1]);

            Static_FSLineRenderer.AppendGrid(props, gridBounds, 1, false);

            Static_FSLineRenderer.Apply(true);

        }
        get { return _pos2; }
    }

    public void NonEditableGrid(Vector3Int Pos1, Vector3Int Pos2) {

        SelPos1CubeNonEditable.transform.position = Pos1;
        SelPos2CubeNonEditable.transform.position = Pos2;
        Vector3[] bounds = CalcBounds(SelPos1CubeNonEditable.transform.position + new Vector3(-0.5f, -0.5f, -0.5f), SelPos2CubeNonEditable.transform.position + new Vector3(-0.5f, -0.5f, -0.5f));
        Debug.Log(bounds[0] + " " + bounds[1]);
        CuboidGridNonEditable.Draw3dGrid(bounds[0], bounds[1],
            new Color32((byte)UnityEngine.Random.Range(0, 20),
            (byte)UnityEngine.Random.Range(0, 10),
            (byte)UnityEngine.Random.Range(111, 244),
            (byte)175),
            FSLineRendereronEditable);
    }

    string invalidCmd = "<color=#" + Console.ccError + "> Invalid Parameter</color>: ";
    string warningCmd = "<color=#" + Console.ccWarning + "> Warning</color>: ";

    string v_setblockInfo = "SetBlock {x, y, z}[optional], blocktype \\ Setblock stone | Setblock 1 1 1 stone";
    string v_setposInfo = "Set position 1 & 2, other voxel commands use these positions to fill area inbetween.";
    string v_stackInfo = "Stack amount, {direction}[cameraDirection = default] \\ Stack 5 | Stack 5 up";
    string v_undoInfo = "Undo the last voxel worldedit opperation.";
    string v_redoInfo = "Redo the last voxel worldedit opperation.";
    string v_lineInfo = "Draws a line from pos1 to pos2\\ v_line stone";
    string v_sizeInfo = "Shows selection size info.";
    string v_cutInfo = "Cuts a selection.";
    string v_pasteInfo = "Pastes a selection.";
    string v_saveInfo = "Saves a cut or copied block selection \\ Save <filename>";
    string v_loadInfo = "Loads a voxel selection. \\ Load <filename>";
    string v_copyInfo = "Copy a selection.";
    string v_flipInfo = "Flip a selection [v]ertically/[h]orizontally \\ Flip v | flip h";
    string v_rotateInfo = "Rotate a selection \\ rotate 90 | rotate 180";

    void Awake() {
        JsonConvert.DefaultSettings().Converters.Add(new Vector3IntConverter());

        Debug.Log(_pos1);


        _mainCam = Camera.main;
        SelPos1Cube = Instantiate(SelPosCube);
        SelPos1Cube.GetComponent<Renderer>().materials[0].SetColor("_Color", Console.hexColor(231, 77, 8, 123));
        SelPos2Cube = Instantiate(SelPosCube);
        SelPos2Cube.GetComponent<Renderer>().materials[0].SetColor("_Color", Console.hexColor(40, 88, 230, 101));

        SelPos1CubeNonEditable = Instantiate(SelPosCube);
        SelPos1CubeNonEditable.GetComponent<Renderer>().materials[0].SetColor("_Color", Console.hexColor(177, 150, 61, 123));
        SelPos2CubeNonEditable = Instantiate(SelPosCube);
        SelPos2CubeNonEditable.GetComponent<Renderer>().materials[0].SetColor("_Color", Console.hexColor(65, 91, 111, 101));

        Static_FSLineRenderer = FSLineRenderer;
        //  CuboidGrid.Static_FSLineRenderer = FSLineRenderer;

        //CuboidGridNonEditable.Static_FSLineRenderer = FSLineRendereronEditable;
        //V_3DGrid.Static_FSLineRenderer = FSLineRenderer;

        Console.AddCommand("v_setblock", Console.dash + v_setblockInfo, v_setblock);
        Console.AddCommand("set", Console.dash + v_setblockInfo, v_setblock); //Alias
        Console.AddCommand("v_pos1", Console.dash + v_setblockInfo, v_pos1);
        Console.AddCommand("pos1", Console.dash + v_setposInfo, v_pos1); //Alias
        Console.AddCommand("v_pos2", Console.dash + v_setblockInfo, v_pos2);
        Console.AddCommand("pos2", Console.dash + v_setposInfo, v_pos2); //Alias
        Console.AddCommand("v_undo", Console.dash + v_undoInfo, v_undo);
        Console.AddCommand("undo", Console.dash + v_undoInfo, v_undo); //Alias
        Console.AddCommand("v_redo", Console.dash + v_redoInfo, v_redo);
        Console.AddCommand("redo", Console.dash + v_redoInfo, v_redo); //Alias
        Console.AddCommand("v_line", Console.dash + v_lineInfo, v_line);
        Console.AddCommand("line", Console.dash + v_lineInfo, v_line); //Alias
        Console.AddCommand("v_size", Console.dash + v_sizeInfo, v_size);
        Console.AddCommand("size", Console.dash + v_sizeInfo, v_size); //Alias
        Console.AddCommand("v_copy", Console.dash + v_copyInfo, v_copy);
        Console.AddCommand("copy", Console.dash + v_copyInfo, v_copy); //Alias
        Console.AddCommand("v_cut", Console.dash + v_cutInfo, v_cut);
        Console.AddCommand("cut", Console.dash + v_cutInfo, v_cut); //Alias
        Console.AddCommand("v_paste", Console.dash + v_pasteInfo, v_paste);
        Console.AddCommand("paste", Console.dash + v_pasteInfo, v_paste); //Alias
        Console.AddCommand("v_save", Console.dash + v_saveInfo, v_save);
        Console.AddCommand("save", Console.dash + v_saveInfo, v_save); //Alias
        Console.AddCommand("v_load", Console.dash + v_loadInfo, v_load);
        Console.AddCommand("load", Console.dash + v_loadInfo, v_load); //Alias
        Console.AddCommand("v_flip", Console.dash + v_flipInfo, v_flip);
        Console.AddCommand("flip", Console.dash + v_flipInfo, v_flip); //Alias
        Console.AddCommand("v_rotate", Console.dash + v_rotateInfo, v_rotate);
        Console.AddCommand("rotate", Console.dash + v_rotateInfo, v_rotate); //Alias
        //Console.AddCommand("v_stack", Console.dash + v_setblockInfo, v_setblock);
    }

    public Vector3 World2Screen(Vector3 point) {
        Vector3 screenPos = _mainCam.WorldToViewportPoint(point);
        screenPos.x *= Screen.width;
        screenPos.y = (1 - screenPos.y) * Screen.height;
        return screenPos;
    }



    void Start() {

        //SelPos2Cube.name = "SelPos1Cube";
        // SelPos1Cube.GetComponent<Renderer>().materials[0].SetColor("_Color", new Color(231, 77, 8, 123));
        // SelPos1Cube.GetComponent<Renderer>().materials[0].SetColor("_Color", Console.hexColor(231, 77, 8, 123));
        //SelPos2Cube.GetComponent<Renderer>().materials[0].color = Console.hexColor(231, 77, 8, 123);
        /*
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        MeshRenderer renderer;

            float r = Random.Range(0.0f, 1.0f);
            float g = Random.Range(0.0f, 1.0f);
            float b = Random.Range(0.0f, 1.0f);
            props.SetColor("_Color", new Color(r, g, b));

            renderer = SelPos1Cube.GetComponent<MeshRenderer>();
            renderer.SetPropertyBlock(props);
        */


        //SelPos2Cube.name = "SelPos2Cube";
        //SelPos2Cube.GetComponent<Renderer>().materials[0].SetColor("_Color", new Color(40, 88, 230, 101));

        // SelPos2Cube.GetComponent<Renderer>().materials[0].color = Console.hexColor(40, 88, 230, 101);

    }

    string v_sel(string param) {

        return "";
    }


    string v_line(string param) {
        if (string.IsNullOrEmpty(param)) {

            return "Need to enter in a block type, eg 'stone' or id #num";
        }
        if (param.Contains("?")) {
            return "Draws a line from pos1 to pos2";
        }
        if (param.Contains("default")) {
            return "";
        }
        string[] array = param.Split();
        ushort type = VE.worldRef.blockProvider.GetType(array[0]);

        // the 0.01f works for shorter distances
        // but you may want to tweak it since the function is
        // unaware of distances
        WE_InProgress = true;
        BlockData bType = new BlockData(type);
        for (float i = 0; i <= 1; i += 0.01f) {
            // parametric line equation 'i' range is from [0 - 1]
            var xi = Pos1.x + ((Pos2.x - Pos1.x) * i);
            var yi = Pos1.y + ((Pos2.y - Pos1.y) * i);
            var zi = Pos1.z + ((Pos2.z - Pos1.z) * i);
            VM.SetBlock(VE.worldRef, new Vector3(xi, yi, zi), bType);
        }
        WE_InProgress = false;
        return "";

    }



    #region v_setpos1&2
    string v_pos1(string param) {
        if (string.IsNullOrEmpty(param)) {
            camPosition = VM.Raycast(new Ray(VE.camRef.transform.position, Vector3.down), VE.worldRef, 0, false);
            Pos1 = camPosition.vector3Int;
            return "SelPos1 set to: " + Pos1;
        }
        if (param.Contains("?")) {
            return "SelPos1 set to: " + Pos1;
        }
        if (param.Contains("default")) {
            return "";
        }
        string[] array = param.Split();
        int x, y, z;
        if (int.TryParse(array[0], out x) && int.TryParse(array[1], out y) && int.TryParse(array[2], out z)) {
            Pos1 = new Vector3Int(x, y, z);
        }
        return "SelPos1 set to: " + Pos1;
    }

    string v_pos2(string param) {
        if (string.IsNullOrEmpty(param)) {
            camPosition = VM.Raycast(new Ray(VE.camRef.transform.position, Vector3.down), VE.worldRef, 0, false);
            Pos2 = camPosition.vector3Int;
            return "SelPos2 set to: " + Pos2;
        }
        if (param.Contains("?")) {
            return "SelPos2 set to: " + Pos2;
        }
        if (param.Contains("default")) {
            return "";
        }
        string[] array = param.Split();
        int x, y, z;
        if (int.TryParse(array[0], out x) && int.TryParse(array[1], out y) && int.TryParse(array[2], out z)) {
            Pos2 = new Vector3Int(x, y, z);
        }
        return "SelPos2 set to: " + Pos2;
    }
    #endregion

    string v_undo(string param) {
        if (param.Contains("?")) {
            return "Undo history: " + _Undo.Count.ToString();
        }
        if (param.Contains("default")) {
            return "Undo history: " + _Undo.Count.ToString();
        }

        string[] array = param.Split();
        if (array[0] == "list" || array[0] == "l") {
            Debug.Log(" REDO.Count:" + _Redo.Count + "UNDO.Count:" + _Undo.Count);
            return "";
        }
        if (array[0] == "clear" || array[0] == "c") {
            _Redo.Clear();
            _Undo.Clear();
            Debug.Log(" REDO.Count:" + _Redo.Count + "UNDO.Count:" + _Undo.Count);
        }

        int count = _Undo.Count;
        int a;
        if (count > 0) {
#if WATCH
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
#endif
            if (_Redo.Count == 0 && !_undoR) {
                setBlocks(new BlockData(0), false, false, true);
                _undoCurrentPos = count;
                _undoR = true;
                Debug.Log("UNDO: Added current stuff to redo stack");
            }

            if (int.TryParse(array[0], out a)) {
                WE_InProgress = true;
                int b = 0;
                foreach (var item in _Undo) {
                    if (a == b) {
                        for (int i = 0; i < item.blocks.Length - 1; i++) {
                            VM.SetBlock(VE.worldRef, item.blocks[i].pos, new BlockData(item.blocks[i].type));
                        }
                    }
                }
                WE_InProgress = false;
#if WATCH
                sw.Stop();
                Debug.Log("UNDO: blockcount: " + _tempUndo.blockCount + "\\ TimeTaken" + sw.Elapsed.TotalMilliseconds + "ms");
#endif
                return "";
            }

            Debug.Log("UNDO: _undoPosition:" + _undoCurrentPos + " REDO.Count:" + _Redo.Count + "UNDO.Count:" + count);

            _tempUndo = _Undo.Pop();
            WE_InProgress = true;
            for (int i = 0; i < _tempUndo.blockCount; i++) {
                VM.SetBlock(VE.worldRef, _tempUndo.blocks[i].pos, new BlockData(_tempUndo.blocks[i].type));
            }
            WE_InProgress = false;
            _Redo.Push(_tempUndo);
#if WATCH
            sw.Stop();
            Debug.Log("UNDO: blockcount: " + _tempUndo.blockCount + "\\ TimeTaken" + sw.Elapsed.TotalMilliseconds + "ms");
#endif
        } else {
            return "Nothing to undo";
        }
        return "";
    }


    string v_redo(string param) {
        if (param.Contains("?")) {
            return "Redo history: " + _Redo.Count.ToString();
        }
        if (param.Contains("default")) {
            return "Redo history: " + _Redo.Count.ToString();
        }
        string[] array = param.Split();
        if (array[0] == "list" || array[0] == "l") {
            Debug.Log(" REDO.Count:" + _Redo.Count + "UNDO.Count:" + _Undo.Count);
            return "";
        }
        int a;
        int count = _Redo.Count;
        if (count > 0) {
#if WATCH
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
#endif
            if (int.TryParse(array[0], out a)) {
                WE_InProgress = true;
                int b = 0;
                foreach (var item in _Redo) {
                    if (a == b) {
                        for (int i = 0; i < item.blocks.Length - 1; i++) {
                            VM.SetBlock(VE.worldRef, item.blocks[i].pos, new BlockData(item.blocks[i].type));
                        }
                    }
                }

                WE_InProgress = false;
#if WATCH
                sw.Stop();
                Debug.Log("blockcount: " + _tempUndo.blockCount + "\\ TimeTaken" + sw.Elapsed.TotalMilliseconds + "ms");
#endif
                return "";
            }
            Debug.Log("REDO: _undoPosition:" + _undoCurrentPos + " REDO.Count:" + count + "UNDO.Count:" + _Undo.Count);
            _tempUndo = _Redo.Pop();
            for (int i = 0; i < _tempUndo.blockCount; i++) {
                VM.SetBlock(VE.worldRef, _tempUndo.blocks[i].pos, new BlockData(_tempUndo.blocks[i].type));
            }
            _Undo.Push(_tempUndo);
            if (_Redo.Count == 0 && _undoR && _undoCurrentPos < _Undo.Count) {
                Debug.Log("REDO Pop last Undo stack");
                _Undo.Pop();
                _undoCurrentPos = 0;
                _undoR = false;
            }

#if WATCH
            sw.Stop();
            Debug.Log("blockcount: " + _tempUndo.blockCount + "\\ TimeTaken" + sw.Elapsed.TotalMilliseconds + "ms");
#endif
        }


        return "";
    }
    string v_cut(string param) {
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }
        if (_pos1Default && _pos2Default) {
            ushort type = VE.worldRef.blockProvider.GetType("air");
            setBlocks(new BlockData(type), true, false, false);
            return "";
        } else {
            return "Need to set position for pos1 and/or pos2 before using this command";
        }
    }

    string v_copy(string param) {
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }
        if (_pos1Default && _pos2Default) {
            //ushort type = VE.worldRef.blockProvider.GetType("air");
            setBlocks(new BlockData(0), false, true, false);
            return "";
        } else {
            return "Need to set position for pos1 and/or pos2 before using this command";
        }

    }

    string v_rotate(string param) {
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }

        if (InMemorySelection == null) {
            return "You need to copy a selection before doing opperations";
        }
        V_Selection tempSelection = InMemorySelection;
#if WATCH
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
#endif
        string[] array = param.Split();
        int angle;
        if (int.TryParse(array[0], out angle)) {
            if (angle % 90 != 0) {
                return "Angle has to be devisible by a 90degree angle";
            }/* else {
                Debug.Log("Angle is accepted");
            }*/
        }

        if (array.Length >= 1 && array[0].StartsWith("360p")) {
            Console.EvalInternal("rotate 90");
            Console.EvalInternal("paste");
            Console.EvalInternal("rotate 90");
            Console.EvalInternal("paste");
            Console.EvalInternal("rotate 90");
            Console.EvalInternal("paste");
            /*
            Console.Eval("rotate 90", false);
            Console.Eval("paste", false);
            Console.Eval("rotate 90", false);
            Console.Eval("paste", false);
            Console.Eval("rotate 90", false);
            Console.Eval("paste", false);
            */
        }
        int x, y, z;
        int i = 0;

        Vector3Int originORplayerpos = InMemorySelection.savePos;

        //Rotate selection based on center origin instead of savePos.
        if (array.Length >= 2 && array[1].StartsWith("o")) {
            var newX = (int)((InMemorySelection.pos1.x + InMemorySelection.pos2.x) / 2);
            var newY = (int)((InMemorySelection.pos1.y + InMemorySelection.pos2.y) / 2);
            var newZ = (int)((InMemorySelection.pos1.z + InMemorySelection.pos2.z) / 2);
            originORplayerpos = new Vector3Int(newX, newY, newZ);
        }
        float angleInRadians = (float)(angle * (Math.PI / 180));
        float cosTheta = Mathf.Cos(angleInRadians);
        float sinTheta = Mathf.Sin(angleInRadians);
        for (x = 0; x < InMemorySelection.xDistance; x++) {
            for (y = 0; y < InMemorySelection.yDistance; y++) {
                for (z = 0; z < InMemorySelection.zDistance; z++) {
                    tempSelection.blocks[i].pos = RotatePoint(tempSelection.blocks[i].pos, originORplayerpos, cosTheta, sinTheta);
                    i++;
                }
            }
        }
        tempSelection.pos1 = (Vector3Int)RotatePoint(tempSelection.pos1, originORplayerpos, cosTheta, sinTheta);
        tempSelection.pos2 = (Vector3Int)RotatePoint(tempSelection.pos2, originORplayerpos, cosTheta, sinTheta);
        NonEditableGrid(tempSelection.pos1, tempSelection.pos2);
        InMemorySelection = tempSelection;
#if WATCH
        sw.Stop();
        Debug.Log("Rotate TimeTaken" + sw.Elapsed.TotalMilliseconds + "ms");
#endif
        return "Rotated <dir>, try pasting now";
    }

    private Vector3 RotatePoint(Vector3Int pointToRotate, Vector3Int centerPoint, float cosTheta, float sinTheta) {
        return new Vector3((cosTheta * (pointToRotate.x - centerPoint.x) - sinTheta * (pointToRotate.z - centerPoint.z) + centerPoint.x),
                pointToRotate.y,
                (sinTheta * (pointToRotate.x - centerPoint.x) + cosTheta * (pointToRotate.z - centerPoint.z) + centerPoint.z));

    }

    string v_flip(string param) {
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }

        if (InMemorySelection == null) {
            return "You need to copy a selection before doing opperations";
        }
        V_Selection tempSelection = InMemorySelection;
        string[] array = param.Split();
        //if (array[0].StartsWith("v", true, System.Globalization.CultureInfo.CurrentCulture) || array[0].Contains("vertical")) { }
        if (array[0].StartsWith("h", true, System.Globalization.CultureInfo.CurrentCulture) || array[0].Contains("horizontal")) {
            // int xDistance = (int)(Math.Sqrt((InMemorySelection.pos1.x - InMemorySelection.pos2.x) * (InMemorySelection.pos1.x - InMemorySelection.pos2.x)));
            // int yDistance = (int)(Math.Sqrt((InMemorySelection.pos1.y - InMemorySelection.pos2.y) * (InMemorySelection.pos1.y - InMemorySelection.pos2.y)));
            // int zDistance = (int)(Math.Sqrt((InMemorySelection.pos1.z - InMemorySelection.pos2.z) * (InMemorySelection.pos1.z - InMemorySelection.pos2.z)));
            int x, y, z;
            int i = 0;

            float originORplayerpos;
            //flip selection based on center origin or flip based on player selection savepos.
            if (array.Length >= 2 && array[1].StartsWith("o")) {
                originORplayerpos = ((InMemorySelection.pos1.x + InMemorySelection.pos2.x) / 2);
            } else {
                originORplayerpos = InMemorySelection.savePos.x;
                var pos1 = new Vector3Int(((InMemorySelection.savePos.x - InMemorySelection.pos1.x) * 2) - 1, InMemorySelection.pos1.y, InMemorySelection.pos1.z);
                var pos2 = new Vector3Int(((InMemorySelection.savePos.x - InMemorySelection.pos2.x) * 2) - 1, InMemorySelection.pos2.y, InMemorySelection.pos2.z);
                InMemorySelection.pos1 = pos1;
                InMemorySelection.pos2 = pos2;
            }

            for (x = 0; x < InMemorySelection.xDistance; x++) {
                for (y = 0; y < InMemorySelection.yDistance; y++) {
                    for (z = 0; z < InMemorySelection.zDistance; z++) {
                        var xP = (int)((originORplayerpos - tempSelection.blocks[i].pos.x) * 2) - 1;
                        tempSelection.blocks[i].pos = new Vector3Int(tempSelection.blocks[i].pos.x + xP, tempSelection.blocks[i].pos.y, tempSelection.blocks[i].pos.z);

                        i++;
                    }
                }
            }
            //DIsplay temp grid to show flipped section
            NonEditableGrid(InMemorySelection.pos1, InMemorySelection.pos2);
            InMemorySelection = tempSelection;
        }
        return "Flipped <dir> try pasting now";
    }

    string v_paste(string param) {
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }

        int size = InMemorySelection.blocks.Length - 1;
        long now = Globals.Watch.ElapsedMilliseconds;

        string[] array = param.Split();
        if (array[0].StartsWith("o")) {

            Debug.Log("Pasting selection in progress, number of blocks: " + size + 1);
            WE_InProgress = true;

            for (int i = 0; i <= size; i++) {
                // Debug.Log(InMemorySelection.blocks[i].pos+" "+ InMemorySelection.blocks[i].type);
                VM.SetBlock(VE.worldRef, InMemorySelection.blocks[i].pos, new BlockData(InMemorySelection.blocks[i].type));
            }
            WE_InProgress = false;
            return "Completed, time taken: " + TimeSpan.FromMilliseconds(Globals.Watch.ElapsedMilliseconds - now).TotalSeconds;
        }
        for (int i = 0; i <= size; i++) {
            Vector3Int newPos = InMemorySelection.savePos - (Vector3Int)VE.camRef.transform.position;
            //Debug.Log(newPos+" "+ (Vector3Int)VE.camRef.transform.position+" "+ InMemorySelection.blocks[i].pos +"\n"+ (InMemorySelection.blocks[i].pos - newPos));
            VM.SetBlock(VE.worldRef, InMemorySelection.blocks[i].pos - newPos, new BlockData(InMemorySelection.blocks[i].type));

        }
        return "Completed, time taken: " + TimeSpan.FromMilliseconds(Globals.Watch.ElapsedMilliseconds - now).TotalMilliseconds;
    }

    public static string PlatformPath {
        get {
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android) {
                return Application.persistentDataPath;
            } else if (Application.platform == RuntimePlatform.LinuxPlayer ||
                Application.platform == RuntimePlatform.OSXEditor ||
                Application.platform == RuntimePlatform.OSXPlayer ||
                Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.WindowsPlayer) {
                return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar;
            } else {
                return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar;
            }
        }
    }

    string v_save(string param) {
        if (string.IsNullOrEmpty(param)) {
            return "Need to enter filename to save as.";
        }
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }

        if (InMemorySelection != null) {
            /*Flip pos1 & pos2 positions and block order before saving so pos1 is always lowest height,
             * quicker for building structure check not having to reverse array everytime its loaded a prefab structure 
             * this way.
             */
            if (InMemorySelection.pos1.y > InMemorySelection.pos2.y) {
                Pos1 = InMemorySelection.pos2;
                Pos2 = InMemorySelection.pos1;
                setBlocks(new BlockData(0), false, true, false); //Copy selection again.
                Debug.Log("pos1 & pos2 flipped for saving consistancy, pos1 always lower or same height as pos2");
            }

#if UNITY_EDITOR
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(InMemorySelection, Formatting.Indented);
#else
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(InMemorySelection);
#endif

            System.IO.StreamWriter writer;
            System.IO.FileInfo t = new System.IO.FileInfo(PlatformPath + param + ".json");

            if (!t.Exists) {
                writer = t.CreateText();
            } else {
                t.Delete();
                writer = t.CreateText();
            }
            writer.Write(json);
            writer.Close();

            return "Saved file: " + PlatformPath + "saveSelection.json";
        }

        return "Need to cut or copy a selection into memory before saving.";

    }
    string v_load(string param) {
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }
        try {
            if (File.Exists(PlatformPath + param + ".json")) {
                // using (var fs = File.OpenRead(PlatformPath + param + ".json")) {              
                //InMemorySelection = new V_Selection.Selection();
                //  InMemorySelection = JsonConvert.DeserializeObject<V_Selection.Selection>(File.ReadAllText(PlatformPath + param + ".json"), new Vector3IntConverter(typeof(Vector3Int)));
                InMemorySelection = JsonConvert.DeserializeObject<V_Selection>(File.ReadAllText(PlatformPath + param + ".json"));

                // }
                return "Selection Loaded. Block Count: " + InMemorySelection.blocks.Length;
            } else {
                Debug.Log("voxel load: Could not find file at path.. \n" + PlatformPath + param + ".json");
            }
        } catch (System.Exception e) {
            Debug.Log("voxel load: " + e);
        }

        return "";

    }
    string v_size(string param) {
        if (string.IsNullOrEmpty(param)) {
            int xDistance = (int)(Mathf.Sqrt((_pos1.x - _pos2.x) * (_pos1.x - _pos2.x)));
            int yDistance = (int)(Mathf.Sqrt((_pos1.y - _pos2.y) * (_pos1.y - _pos2.y)));
            int zDistance = (int)(Mathf.Sqrt((_pos1.z - _pos2.z) * (_pos1.z - _pos2.z)));
            int total = (xDistance + 1) * (yDistance + 1) * (zDistance + 1);
            return "Total size: " + total + "// x Dist: " + xDistance + ", y Dist: " + yDistance + ", z Dist: " + zDistance;
        }
        if (param.Contains("?")) {
            return "Return: " + Pos2;
        }
        if (param.Contains("default")) {
            return "";
        }
        return "";
    }



    void setBlocks(BlockData blockType, bool cutblock, bool copyblock, bool undocopy) {

        int xDistance = (int)(Math.Sqrt((_pos1.x - _pos2.x) * (_pos1.x - _pos2.x))) + 1;
        int yDistance = (int)(Math.Sqrt((_pos1.y - _pos2.y) * (_pos1.y - _pos2.y))) + 1;
        int zDistance = (int)(Math.Sqrt((_pos1.z - _pos2.z) * (_pos1.z - _pos2.z))) + 1;
        //if (xDistance == 0) { xDistance += 1; }
        //if (yDistance == 0) { yDistance += 1; }
        //if (zDistance == 0) { zDistance += 1; }

        //Block[,,] SelectionArray = new Block[xDistance-1, yDistance-1, zDistance-1];
        //ushort[,,] SelectionArray = new ushort[xDistance, yDistance, zDistance];

        //SelectionArray[0, 0, 0] = VM.GetBlock(VE.worldRef, new Vector3Int(SelPos1.x, SelPos1.y, SelPos1.z));
        int totalSize = (xDistance) * (yDistance) * (zDistance);
        Debug.Log("Area Size: " + totalSize + " \\\\ xDist(" + (xDistance) + ") * yDist(" + (yDistance) + ") * zDist(" + (zDistance) + ")");

        // for (int i = 1; i < total; i++) {
#if WATCH
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
#endif
        V_Selection BlockSelection = new V_Selection();
        BlockSelection.blocks = new V_Selection.Block[totalSize];
        BlockSelection.pos1 = Pos1;
        BlockSelection.pos2 = Pos2;
        BlockSelection.xDistance = xDistance;
        BlockSelection.yDistance = yDistance;
        BlockSelection.zDistance = zDistance;
        BlockSelection.blockCount = totalSize;

        Vector3Int tempPos;
        WE_InProgress = true;
        int x = 0;
        int y = 0;
        int z = 0;
        int i = 0;
        for (y = 0; y < yDistance; y++) { //from bottom > up
            for (x = 0; x < xDistance; x++) {
                for (z = 0; z < zDistance; z++) {
                    /*
                    for (x = 0; x < xDistance; x++) {
                        for (y = 0; y < yDistance; y++) {
                            for (z = 0; z < zDistance; z++) {
                            */
                    tempPos = new Vector3Int(_pos1.x + ((_pos2.x > _pos1.x) ? x : x * -1),
                                             _pos1.y + ((_pos2.y > _pos1.y) ? y : y * -1),
                                             _pos1.z + ((_pos2.z > _pos1.z) ? z : z * -1));

                    //todo remove getblock, as it is already called again in the setblock code as a callback, so hook into that later.
                    V_Selection.Block blk = new V_Selection.Block();
                    blk.pos = tempPos;
                    blk.type = VM.GetBlock(VE.worldRef, tempPos).type;
                    if (blk.type == 0) {
                        blk.state = V_Selection.blockState.air;
                    } else {
                        blk.state = V_Selection.blockState.block;
                    }
                    BlockSelection.blocks[i] = blk;

                    if (copyblock == false) {
                        VM.SetBlock(VE.worldRef, tempPos, blockType);
                    }
                    i++;
                }
            }
        }
        if (cutblock || copyblock) {
            InMemorySelection = BlockSelection;
            Vector3Int tmp = VM.Raycast(new Ray(VE.camRef.transform.position, Vector3.down), VE.worldRef, 0, false).vector3Int;
            InMemorySelection.savePos = new Vector3(tmp.x, tmp.y, tmp.z);
        }

        WE_InProgress = false;
        if (undocopy) {
            _Redo.Push(BlockSelection);
            Debug.Log("undocopy  _Undo.Count: " + _Undo.Count + " _Redo.Count:" + _Redo.Count);
        }
        if (!undocopy && !copyblock) {

            _Undo.Push(BlockSelection);
            _Redo.Clear();// Once we issue a new command, the redo stack clears
            _undoCurrentPos = 0;
            _undoR = false;
            Debug.Log("Setblock _Undo.Count: " + _Undo.Count + " _Redo.Count:" + _Redo.Count);
            //Debug.Log("_undoPosition: " + _undoCurrentPos + " blockUndos.Count:" + blockUndos.Count);

        }
        /*
        if (_undoCurrentPos > 1) {
            NonEditableGrid(blockUndos[_undoCurrentPos - 1].pos1, blockUndos[_undoCurrentPos - 1].pos2);
        }
        */
#if WATCH
        sw.Stop();
        Debug.Log("blockcount: " + i + "\\ TimeTaken" + sw.Elapsed.TotalMilliseconds + "ms");
        sw.Reset();
#endif
        //float distance = distance = Mathf.Sqrt(Mathf.Pow((SelPos1.x - SelPos2.x), 2) + Mathf.Pow((SelPos1.y - SelPos2.y), 2) + Mathf.Pow((SelPos1.z - SelPos2.z), 2));
        //int distanceInt = System.Convert.ToInt32(distance);

        //Vector3Int Area = new Vector3Int(SelPos1.x * SelPos2.x, SelPos1.y * SelPos2.y, SelPos1.z * SelPos2.z);

    }
    string v_setblock(string param) {
        if (string.IsNullOrEmpty(param)) {
            return v_setblockInfo;
        }
        if (param.Contains("?")) {
            return "";
        }
        if (param.Contains("default")) {
            return "";
        }
        string[] array = param.Split();
        int x, y, z;
        bool found;
        if (int.TryParse(array[0], out x) && int.TryParse(array[1], out y) && int.TryParse(array[2], out z)) {
            try {
                ushort type = VE.worldRef.blockProvider.GetType(array[3], out found);
                if (found) {
                    VM.SetBlock(VE.worldRef, new Vector3Int(x, y, z), new BlockData(type));
                }
                return "(" + x + ", " + y + ", " + z + "), Block set: " + type;
            } catch (System.Exception) {
                return invalidCmd + array[1] + " (Not a valid block type)";
            }
        }
        //Todo me <direction amount> <blocktype>
        if (array[0].Equals("me")) {
            ushort type = VE.worldRef.blockProvider.GetType(array[1], out found);
            if (found) {
                camPosition = VM.Raycast(new Ray(VE.camRef.transform.position, Vector3.down), VE.worldRef, 0, type == BlockProvider.AirType);
                VM.SetBlock(VE.worldRef, camPosition.vector3Int, new BlockData(type));
                return "CamPosition: " + VE.camRef.transform.position + ", VecInt: " + camPosition.vector3Int + ", Block set: " + type;
            } else {
                return invalidCmd + array[1] + " (Not a valid block type)";
            }
        }

        if (_pos1Default && _pos2Default) {
            ushort type = VE.worldRef.blockProvider.GetType(param, out found);
            if (found) {
                setBlocks(new BlockData(type), false, false, false);
                return "";
            } else {
                return invalidCmd + param + " (Not a valid block type)";
            }
        } else {
            return "Need to set position for pos1 and/or pos2 before using this command";
        }

        /*
       ushort type = Voxelmetric.Examples.VoxelmetricExample.worldref.blockProvider.GetType(param);
        for (int x = 0; x < 100; x++) {
            for (int y = 0; y < 100; y++) {

                Voxelmetric.Code.Voxelmetric.SetBlock(Voxelmetric.Examples.VoxelmetricExample.worldref, new Vector3Int(x, y, x), new BlockData(type));
            }
        }
        */
        // StartCoroutine(delayedSetblock(0.1f, param));
    }


    public static Vector3[] CalcBounds(Vector3 firstPoint, Vector3 secondPoint) {
        Vector3[] ret = new Vector3[2];
        float off = 0.02F;
        float off1 = 1.0F + off;

        ret[0] = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        ret[1] = new Vector3(float.MinValue, float.MinValue, float.MinValue);
        foreach (Vector3 point in new Vector3[2] { firstPoint, secondPoint }) {
            if (point.x + off1 > ret[1].x) {
                ret[1].x = point.x + off1;
            }
            if (point.x - off < ret[0].x) {
                ret[0].x = point.x - off;
            }
            if (point.y + off1 > ret[1].y) {
                ret[1].y = point.y + off1;
            }
            if (point.y - off < ret[0].y) {
                ret[0].y = point.y - off;
            }
            if (point.z + off1 > ret[1].z) {
                ret[1].z = point.z + off1;
            }
            if (point.z - off < ret[0].z) {
                ret[0].z = point.z - off;
            }
        }
        return ret;
    }



    private IEnumerator delay(float delay) {
        yield return new WaitForSeconds(delay);
    }
    private IEnumerator delayedSetblock(float delay, string block) {


        ushort type = Voxelmetric.Examples.VoxelmetricExample.worldRef.blockProvider.GetType(block);
        for (int x = 100; x < 200; x++) {
            for (int y = 0; y < 200; y++) {
                yield return new WaitForSeconds(delay); //give the script time to submit the correct value before changing data

                Voxelmetric.Code.Voxelmetric.SetBlock(Voxelmetric.Examples.VoxelmetricExample.worldRef, new Vector3Int(x, y, x), new BlockData(type));
            }
        }

    }

}
